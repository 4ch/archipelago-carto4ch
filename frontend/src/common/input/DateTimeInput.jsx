import React from "react";
import frLocale from "date-fns/locale/fr";
import { DateInput } from "@semapps/date-components";

const DateTimeInput = props => (
  <DateInput
    options={{
      format: 'dd/MM/yyyy',
      ampm: false,
      clearable: true
    }}
    providerOptions={{
      locale: frLocale
    }}
    fullWidth
    {...props}
  />
);

export default DateTimeInput;
