import React, { useEffect } from 'react';
import { ReferenceArrayInput as RaReferenceArrayInput } from 'react-admin';
import { useController } from 'react-hook-form';

const ReferenceArrayInput = props => {
  // const record = useRecordContext();
  // console.log("ReferenceArrayInput record=", record);
  // console.log("ReferenceArrayInput props=", props);

  let { field: { value, onChange }} = useController({ name: props.source });

  useEffect(() => {
  if (value && !Array.isArray(value)) {
  // console.log("ReferenceArrayInput useEffect value=", value);
  onChange([value]);
    }
  }, [value, onChange]);

  // const refToKeep = (props.reference+"s").toLowerCase()
  // console.log("refToKeep=", refToKeep);
  // console.log("value avant=", value);
  // value = value.filter(uri => ((uri.split('/'))[3] === refToKeep))
  // console.log("value après=", value);

  // Wait for change to be effective before rendering component
  // Otherwise it will be wrongly initialized and it won't work
  // if (value && !Array.isArray(value)) return null;


  return <RaReferenceArrayInput {...props} />;
};

export default ReferenceArrayInput;