export { default as Layout } from './Layout';

export { default as Create } from './create/Create';
export { default as CreateView } from './create/CreateView';

export { default as Edit } from './edit/Edit';
export { default as EditView } from './edit/EditView';

export { default as List } from './list/List';
export { default as ListView } from './list/ListView';

export { default as Show } from './show/Show';
export { default as ShowView } from './show/ShowView';
