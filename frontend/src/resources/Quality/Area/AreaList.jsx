import React from 'react';
import SimpleList from "../../../common/list/SimpleList";
import AreaIcon from '@mui/icons-material/Category';
import { ListWithPermissions } from '@semapps/auth-provider';

const AreaList = () => (
  <ListWithPermissions>
    <SimpleList primaryText={record => record['pair:label']} leftAvatar={() => <AreaIcon />} linkType="show" />
  </ListWithPermissions>
);

export default AreaList;
