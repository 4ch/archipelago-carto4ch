import React from 'react';
import { TextField } from 'react-admin';
import { Grid, Avatar } from '@mui/material';
import { MainList, SideList } from '../../../common/list';
import { MarkdownField } from '@semapps/markdown-components';
import { ShowWithPermissions } from '@semapps/auth-provider';
import { MultiUrlField, QuickAppendReferenceArrayField } from '@semapps/field-components';
import ChipList from '../../../common/list/ChipList';

const domainMapping = {
  'data.europa.eu': {
    label: 'ESCO',
    icon: <Avatar component="span" src="https://images.freeimages.com/fic/images/icons/658/european_flags/256/european_union.png" />,
    color: 'grey',
    contrastText: 'white'
  }
}

const OccupationShow = () => {
  return(
  <ShowWithPermissions>
    <Grid container spacing={5}>
      <Grid item xs={12} sm={9}>
        <MainList>
          <TextField source="rdfs:comment" />
          <MultiUrlField source="prov:wasDerivedFrom" domainMapping={domainMapping}/>
          <MarkdownField source="pair:description" />
        </MainList>
      </Grid>
      <Grid item xs={12} sm={3}>
        <SideList>
          <QuickAppendReferenceArrayField reference="Job" source="heco:isOccupationOf">
            <ChipList primaryText="pair:label" linkType="edit" externalLinks />
          </QuickAppendReferenceArrayField>
        </SideList>
      </Grid>
    </Grid>
  </ShowWithPermissions>
)};

export default OccupationShow;
