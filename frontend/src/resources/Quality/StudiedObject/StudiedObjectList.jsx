import React from 'react';
import SimpleList from "../../../common/list/SimpleList";
import StudiedObjectIcon from '@mui/icons-material/Category';
import { ListWithPermissions } from '@semapps/auth-provider';

const StudiedObjectList = () => (
  <ListWithPermissions>
    <SimpleList primaryText={record => record['pair:label']} leftAvatar={() => <StudiedObjectIcon />} linkType="show" />
  </ListWithPermissions>
);

export default StudiedObjectList;
