import React from 'react';
import SimpleList from "../../../common/list/SimpleList";
import ToolIcon from '@mui/icons-material/Category';
import { ListWithPermissions } from '@semapps/auth-provider';

const ToolList = () => (
  <ListWithPermissions>
    <SimpleList primaryText={record => record['pair:label']} leftAvatar={() => <ToolIcon />} linkType="show" />
  </ListWithPermissions>
);

export default ToolList;
