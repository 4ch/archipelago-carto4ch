import React from 'react';
import ProjectForm from './ProjectForm';
import { EditWithPermissions } from '@semapps/auth-provider';

const ProjectEdit = props => (
  <EditWithPermissions {...props}>
    <ProjectForm />
  </EditWithPermissions>
);

export default ProjectEdit;
