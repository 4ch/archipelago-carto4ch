import React from 'react';
import TrainingForm from './TrainingForm';
import { EditWithPermissions } from '@semapps/auth-provider';

const TrainingEdit = props => (
  <EditWithPermissions {...props}>
    <TrainingForm />
  </EditWithPermissions>
);

export default TrainingEdit;
