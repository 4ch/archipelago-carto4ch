import React from 'react';
import { Card, CardContent } from '@mui/material';
import { ReferenceFilter } from '@semapps/list-components';
import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles(theme => ({
  card: {
    paddingTop: 0,
    [theme.breakpoints.up('sm')]: {
      minWidth: '15em',
      marginLeft: '1em'
    },
    [theme.breakpoints.down('sm')]: {
      display: 'none'
    }
  },
  cardContent: {
    paddingTop: 0
  }
}));

const CommitmentFilterSidebar = () => {
  const classes = useStyles();
  return (
    <Card className={classes.card}>
      <CardContent className={classes.cardContent}>
        <ReferenceFilter
          reference="Theme"
          source="heco:hasTopic"
          inverseSource="heco:topicOf"
          limit={100}
          sort={{ field: 'pair:label', order: 'DESC' }}
        />
      </CardContent>
    </Card>
  );
};

export default CommitmentFilterSidebar;
