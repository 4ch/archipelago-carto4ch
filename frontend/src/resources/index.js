/* eslint-disable no-redeclare */
import Actor from './Actor/Actor';

// Actors
import Organization from './Actor/Organization';
import Person from './Actor/Person';
import Project from './Actor/Project';

// Qualités
import Quality from './Quality/Quality';
import Discipline from './Quality/Discipline';
import Area from './Quality/Area';
import StudiedObject from './Quality/StudiedObject';
import Tool from './Quality/Tool';
import Occupation from './Quality/Occupation';

// Resources
import Resource from './Resource/Resource';
import Job from './Resource/Job';
import Commitment from './Resource/Commitment';
import Training from './Resource/Training';
import Competence from './Resource/Competence';

// Concepts
import Concept from './Concept/Concept';
import Theme from './Concept/Theme';
import Status from './Concept/Status';
import Type from './Concept/Type';
import Page from './Concept/Page';

const resources = {
  // Actors
  Actor,
  Organization,
  Person,
  Project,

  // Resources
  Resource,
  Job,
  Commitment,
  Training,
  Competence,

  // Qualités
  Quality,
  Discipline,
  Area,
  StudiedObject,
  Tool,
  Occupation,

  // Concepts
  Concept,
  Page,
  Status,
  Theme,
  Type,
};

export default resources;
