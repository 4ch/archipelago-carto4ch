import TypeCreate from './TypeCreate';
import TypeEdit from './TypeEdit';
import TypeList from './TypeList';
import StyleIcon from '@mui/icons-material/Style';

const resource = {
  config: {
    list: TypeList,
    create: TypeCreate,
    edit: TypeEdit,
    icon: StyleIcon,
    options: {
      label: 'app.menu.types',
      parent: 'Concept'
    },
    recordRepresentation: 'pair:label'
  },
  dataModel: {
    types: [
      'pair:Type',
      'pair:ProjectType',
      'pair:ConceptType',
      'pair:OrganizationType',
      'pair:JobType',
      'pair:ResourceType',
      'pair:SubjectType'
    ],
    list: {
      servers: '@default'
    },
    fieldsMapping: {
      title: 'pair:label'
    }
  },
  translations: {
    fr: {
      name: 'Type |||| Types',
      fields: {
        '@type': 'Classe',
        'pair:label': 'Nom'
      }
    },
    en: {
      name: 'Type |||| Types',
      fields: {
        '@type': 'Class',
        'pair:label': 'Name'
      }
    }
  }
};

export default resource;
