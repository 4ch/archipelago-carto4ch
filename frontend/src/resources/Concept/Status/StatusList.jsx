import React from 'react';
import VisibilityIcon from '@mui/icons-material/Visibility';
import SimpleList from "../../../common/list/SimpleList";
import { ListWithPermissions } from '@semapps/auth-provider';

const StatusList = props => (
  <ListWithPermissions {...props}>
    <SimpleList
      primaryText={record => record['pair:label']}
      secondaryText={record => record.type}
      leftAvatar={() => <VisibilityIcon />}
    />
  </ListWithPermissions>
);

export default StatusList;
