import React from 'react';
import { SimpleForm, TextInput, SelectArrayInput } from 'react-admin';
import { CreateWithPermissions } from '@semapps/auth-provider';

const StatusCreate = () => (
  <CreateWithPermissions>
    <SimpleForm>
      <TextInput source="pair:label" fullWidth />
      <SelectArrayInput
        source="@type"
        choices={[
          { id: 'pair:Status', name: "Status" },
          { id: 'pair:ProjectStatus', name: "Statut de projet" },
          { id: 'pair:OrganizationStatus', name: "Statut d'organisation" },
        ]}
      />
    </SimpleForm>
  </CreateWithPermissions>
);

export default StatusCreate;
