import React from 'react';
import { SimpleForm, TextInput } from 'react-admin';
import { MarkdownInput, useLoadLinks } from '@semapps/markdown-components';
import { EditWithPermissions } from '@semapps/auth-provider';

export const PageEdit = () => {
  const loadLinks = useLoadLinks('Page', 'semapps:title');
  return (
    <EditWithPermissions>
      <SimpleForm redirect="show">
        <TextInput source="semapps:title" fullWidth />
        <MarkdownInput source="semapps:content" loadSuggestions={loadLinks} suggestionTriggerCharacters="[" fullWidth />
      </SimpleForm>
    </EditWithPermissions>
  );
};

export default PageEdit;
