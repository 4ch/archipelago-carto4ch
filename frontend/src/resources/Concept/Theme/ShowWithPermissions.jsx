import React from 'react';
import { Show, useGetRecordId } from 'react-admin';
import { ShowActionsWithPermissions, useCheckPermissions } from '@semapps/auth-provider';

const ShowWithPermissions = props => {
  const recordId = useGetRecordId({});
  useCheckPermissions(recordId, 'show');
  return <Show {...props} />;
};

ShowWithPermissions.defaultProps = {
  actions: <ShowActionsWithPermissions />
};

export default ShowWithPermissions;