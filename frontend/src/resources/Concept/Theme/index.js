import CreateOrImport from '../../../common/CreateOrImport';
import ThemeEdit from './ThemeEdit';
import ThemeList from './ThemeList';
import ThemeShow from './ThemeShow';
import LocalOfferIcon from '@mui/icons-material/LocalOffer';

const resource = {
  config: {
    list: ThemeList,
    show: ThemeShow,
    create: CreateOrImport,
    edit: ThemeEdit,
    icon: LocalOfferIcon,
    options: {
      label: 'app.menu.themes',
      parent: 'Concept'
    },
    recordRepresentation: 'pair:label'
  },
  dataModel: {
    types: ['heco:Topic'],
    list: {
      servers: '@default'
    },
    fieldsMapping: {
      title: 'pair:label'
    }
  },
  translations: {
    fr: {
      name: 'Thème |||| Thèmes',
      fields: {
        'pair:label': 'Titre',
        'rdfs:comment': 'Courte description',
        'pair:description': 'Description',
        'heco:topicOf': 'Sujet de'
      }
    },
    en: {
      name: 'Theme |||| Themes',
      fields: {
        'pair:label': 'Title',
        'rdfs:comment': 'Short description',
        'pair:description': 'Description',
        'heco:topicOf': 'Interest Of'
      }
    }
  }
};

export default resource;
