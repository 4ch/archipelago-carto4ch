const { LdpService, DocumentTaggerMixin } = require('@semapps/ldp');
const urlJoin = require('url-join');
const CONFIG = require('../config/config');
const containers = require('../config/containers');

module.exports = {
  mixins: [LdpService, DocumentTaggerMixin],
  settings: {
    baseUrl: CONFIG.HOME_URL,
    containers,
    preferredViewForResource: async (resourceUri, containerPreferredView) => {
      if (!containerPreferredView) return resourceUri;
      return urlJoin(CONFIG.FRONT_URL, containerPreferredView, encodeURIComponent(resourceUri), 'show')
    }
  },
  hooksResource: {
    before: {
      "get":async (ctx, res)=>{
        // console.log('>>>>>>>>>>>>>>>HOOK Before GET res=',res);
        return res;
      }
    },
    after: {
      "get":async (ctx, res)=>{
        // console.log('>>>>>>>>>>>>>>>HOOK After GET ctx / container=',ctx);
        let container = ''
        let containerUri = ctx.options.parentCtx.params.containerUri;
        // Sometime, containerUri is present, but deeper
        if (!containerUri && ctx.options.parentCtx.options.parentCtx)
          containerUri = ctx.options.parentCtx.options.parentCtx.params.containerUri;

        if (containerUri)
          container = await ctx.call('ldp.registry.getByUri', { containerUri : containerUri })
        else {
          // console.log("No containerUri founded")
          return res
        }
  
        if (container && container.path === "/competences") {
          // Split competences in 3 predicates
          const splitUri = {"heco:isAcquiredByJob" : [], 
            "heco:isAcquiredByCommitment": [], 
            "heco:isAcquiredByTraining" : [] 
          };

          // console.log('>>>>>>>>>>>>>>>HOOK After GET res=',res);
          if (res["heco:isCompetenceAcquiredIn"])
          {
            // If not an array (one element) -> make an array
            if (res["heco:isCompetenceAcquiredIn"] && !Array.isArray(res["heco:isCompetenceAcquiredIn"]))
              res["heco:isCompetenceAcquiredIn"] = [ res["heco:isCompetenceAcquiredIn"] ]
            
            res["heco:isCompetenceAcquiredIn"].forEach(element => {
              console.log('Dans compétence, heco:isCompetenceAcquiredIn, element =', element);            
              switch (element.split('/')[3]) {
                case 'jobs':
                  splitUri["heco:isAcquiredByJob"].push(element)
                  break
                case 'commitments':
                  splitUri["heco:isAcquiredByCommitment"].push(element)
                  break
                case 'trainings':
                  splitUri["heco:isAcquiredByTraining"].push(element)
                  break
              }
            })
          }

          // Attention !! Ne pas supprimer la relation isCompetenceAcquiredIn
          // Sinon, lors du PUT, il ne le retrouve pas, et ne peut pas la supprimer !
          // delete res["heco:isCompetenceAcquiredIn"]

          res = {...res, 
            "heco:isAcquiredByJob" : splitUri["heco:isAcquiredByJob"],
            "heco:isAcquiredByCommitment" : splitUri["heco:isAcquiredByCommitment"],
            "heco:isAcquiredByTraining" : splitUri["heco:isAcquiredByTraining"],
          }
           // console.log('res =', res);         
        }
        return res;
      }
    },
    before: {
      "put":async (ctx)=>{
        let container = ''
        const resource = ctx.params.resource;
        // console.log('>>>>>>>>>>>>>>>HOOK Before PUT ctx=',ctx);
        let containerUri = ctx.options.parentCtx.params.containerUri;
        // Sometime, containerUri is present, but deeper
        if (!containerUri && ctx.options.parentCtx.options.parentCtx)
          containerUri = ctx.options.parentCtx.options.parentCtx.params.containerUri;

        if (containerUri)
          container = await ctx.call('ldp.registry.getByUri', { containerUri : containerUri })
        else {
          // console.log("No containerUri founded")
        }
  
        // if (containerUri.substring(containerUri.lastIndexOf('/') + 1) === "competences") {
          if (container && container.path === "/competences") {
          resource["heco:isCompetenceAcquiredIn"] = [].concat(
            resource["heco:isAcquiredByJob"], 
            resource["heco:isAcquiredByCommitment"], 
            resource["heco:isAcquiredByTraining"]
          )

          delete resource["heco:isAcquiredByJob"]
          delete resource["heco:isAcquiredByCommitment"]
          delete resource["heco:isAcquiredByTraining"]
          console.log('resource =', resource);         
        }

        ctx.params.resource = resource
      },
      "create":async (ctx)=>{
        let container = ''
        const resource = ctx.params.resource;
        // console.log('>>>>>>>>>>>>>>>HOOK Before CREATE ctx=',ctx);
        let containerUri = ctx.options.parentCtx.params.containerUri;
        // Sometime, containerUri is present, but deeper
        if (!containerUri && ctx.options.parentCtx.options.parentCtx)
          containerUri = ctx.options.parentCtx.options.parentCtx.params.containerUri;

        if (containerUri)
          container = await ctx.call('ldp.registry.getByUri', { containerUri : containerUri })
        else {
          // console.log("No containerUri founded")
        }
  
        // if (containerUri.substring(containerUri.lastIndexOf('/') + 1) === "competences") {
          if (container && container.path === "/competences") {
          resource["heco:isCompetenceAcquiredIn"] = [].concat(
            resource["heco:isAcquiredByJob"], 
            resource["heco:isAcquiredByCommitment"], 
            resource["heco:isAcquiredByTraining"]
          )

          delete resource["heco:isAcquiredByJob"]
          delete resource["heco:isAcquiredByCommitment"]
          delete resource["heco:isAcquiredByTraining"]
          console.log('resource =', resource);         
        }

        ctx.params.resource = resource
      }
    },
  }
};
