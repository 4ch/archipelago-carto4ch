const path = require('path');
const urlJoin = require("url-join");
const { CoreService } = require('@semapps/core');
const CONFIG = require('../config/config');
const containers = require('../config/containers');
const { pair, semapps, og, as } = require('@semapps/ontologies')

module.exports = {
  mixins: [CoreService],
  settings: {
    baseUrl: CONFIG.HOME_URL,
    baseDir: path.resolve(__dirname, '..'),
    ontologies : [pair, semapps, og, as, 
      {
        prefix: "heco",
        namespace: "https://portal.carto4ch.huma-num.fr/ontologies/heco#",
        owl: "https://gitlab.huma-num.fr/4ch/heco/-/raw/main/heco.owl",
        jsonldContext: "https://gitlab.huma-num.fr/4ch/heco/-/raw/main/context-jsonld/heco.json",
        preserveContextUri: false,
        overwrite: false
      }
    ],
    triplestore: {
      url: CONFIG.SPARQL_ENDPOINT,
      user: CONFIG.JENA_USER,
      password: CONFIG.JENA_PASSWORD,
      mainDataset: CONFIG.MAIN_DATASET,
      fusekiBase: CONFIG.SEMAPPS_FUSEKI_BASE,
    },
    containers,
    mirror: true,
    // jsonld: false, // Custom configuration in jsonld.service.js file
    ldp: false, // Custom configuration in ldp.service.js file
    // Sub-services settings
    activitypub: {
      activitiesPath: "/activities"
    },
    api: {
      port: CONFIG.PORT,
    },
    ldp: {
      preferredViewForResource: async (resourceUri, containerPreferredView) => {
        if (!containerPreferredView) return resourceUri;
        return urlJoin(CONFIG.FRONT_URL, containerPreferredView, encodeURIComponent(resourceUri), 'show')
      }
    },
    void: {
      title: CONFIG.INSTANCE_NAME,
      description: CONFIG.INSTANCE_DESCRIPTION
    },
    webacl: {
      superAdmins: []
    },
    webid: {
      path: 'users',
    },
  }
};
