const CONFIG = require('./config');
const { ACTOR_TYPES } = require("@semapps/activitypub");

module.exports = [
  {
    path: '/'
  },
  {
    path: '/organizations',
    acceptedTypes: ['heco:Organization'],
    preferredView: '/Organization',
    dereference: ['sec:publicKey', 'pair:hasLocation/pair:hasPostalAddress'],
  },
  {
    path: '/projects',
    acceptedTypes: ['heco:Project'],
    preferredView: '/Project',
  },
  {
    path: '/jobs',
    preferredView: '/Job',
    acceptedTypes: ['heco:Job']
  },
  {
    path: '/trainings',
    preferredView: '/Training',
    acceptedTypes: ['heco:Training']
  },
  {
    path: '/commitments',
    preferredView: '/Commitment',
    acceptedTypes: ['heco:Commitment']
  },
  {
    path: '/disciplines',
    preferredView: '/Discipline',
    acceptedTypes: ['heco:Discipline']
  },
  {
    path: '/areas',
    preferredView: '/Area',
    acceptedTypes: ['heco:Area']
  },
  {
    path: '/studied-objects',
    preferredView: '/StudiedObject',
    acceptedTypes: ['heco:StudiedObject']
  },
  {
    path: '/tools',
    preferredView: '/Tool',
    acceptedTypes: ['heco:Tool']
  },
  {
    path: '/competences',
    preferredView: '/Competences',
    acceptedTypes: ['heco:Competence']
  },
  {
    path: '/users',
    preferredView: '/Person',
    acceptedTypes: ['heco:Person'],
    dereference: ['sec:publicKey', 'pair:hasLocation/pair:hasPostalAddress'],
  },
  {
    path: '/bots',
    acceptedTypes: [ACTOR_TYPES.APPLICATION],
    dereference: ['sec:publicKey'],
    excludeFromMirror: true
  },
  {
    path: '/themes',
    preferredView: '/Theme',
    acceptedTypes: ['heco:Topic']
  },
  {
    path: '/occupations',
    preferredView: '/Occupation',
    acceptedTypes: ['heco:Occupation']
  },
  {
    path: '/status',
    preferredView: '/Status',
    acceptedTypes: [
      'pair:Status',
      'pair:ProjectStatus',
      'pair:OrganizationStatus'
    ]
  },
  {
    path: '/types',
    preferredView: '/Type',
    acceptedTypes: [
      'pair:Type',
      'pair:ProjectType',
      'pair:OrganizationType',
      'pair:JobType',
    ]
  },
  {
    path: '/pages',
    preferredView: '/Page',
    acceptedTypes: ['semapps:Page']
  }
];
